package assignment1Material;

import assignment1Stock.Inventory;
public class Laptops extends Inventory{
	public Laptops(int uid)
	{
		super(uid);
		laptop_quantity+=1;
	}
	
	public void display()
	{
		System.out.println("1 TB HDD");
		System.out.println("8-GB RAM");
		System.out.println("250 GB SDD");
		System.out.println("No Graphics Card");
		System.out.println("4-USB Ports");
	}

}
