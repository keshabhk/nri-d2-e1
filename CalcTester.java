package assignment1;

public class CalcTester {
	public static void main(String args[])
	{
		//create an object of Calculator.
		Calculator obj = new Calculator();
		
		System.out.println("Avg of 3 numbers: "+obj.findAverage(70, 60, 90));
		System.out.println("Avg of 4 numbers: "+obj.findAverage(13, 67, 45, 76));
		System.out.println("Avg of 5 numbers: "+obj.findAverage(78, 34, 77, 90, 12));
		
	}
}
