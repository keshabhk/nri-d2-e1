package assignment1;

public class Calculator {
	public double findAverage(int a,int b,int c)
	{
		return Math.round(((a+b+c)/3)*100.0)/100.0;
		
	}
	
	public double findAverage(int a,int b,int c,int d)
	{
		return Math.round(((a+b+c+d)/3)*100.0)/100.0;
		
	}
	
	public double findAverage(int a,int b,int c,int d,int e)
	{
		return Math.round(((a+b+c+d+e)/3)*100.0)/100.0;
		
	}

}
