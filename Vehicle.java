package assignment1;

public class Vehicle {
	
	String color;
	String model;
	int noOfWheels;

	//constructors
	public Vehicle(String color, String model, int noOfWheels) {
		super();
		this.color = color;
		this.model = model;
		this.noOfWheels = noOfWheels;
	}
	
	void info()
	{
		System.out.println("Color: "+this.color);
		System.out.println("Model: "+this.model);
		System.out.println("No of wheels: "+this.noOfWheels);
	}

}

class Bus extends Vehicle{
	
	public Bus(String color, String model, int noOfWheels)
	{
		super(color,model,noOfWheels);
	}
	
}

class Truck extends Vehicle{
	public Truck(String color, String model, int noOfWheels)
	{
		super(color,model,noOfWheels);
	}
	
}

class Car extends Vehicle{
	public Car(String color, String model, int noOfWheels)
	{
		super(color,model,noOfWheels);
	}
	
	
}

