package assignment1;

public class Road {

	public static void main(String args[])
	{
		Bus b = new Bus("Yellow","Single Deck",4);
		Truck t = new Truck("Red","Cargo",8);
		Car c = new Car("Black","SUV",4);
		
		System.out.println("--Bus--");
		b.info();
		System.out.println("\n--Truck--");
		t.info();
		System.out.println("\n--Car--");
		c.info();
	}
}
