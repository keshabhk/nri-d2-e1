package assignment1Stock;

import assignment1Material.Laptops;
import assignment1Material.Accessories;
import java.util.Scanner;

public class Order {
	public static void main(String args[])
	{
		//create laptop objects and accessories objects.
		Laptops l1 = new Laptops(101);
		Laptops l2 = new Laptops(102);
		Laptops l3 = new Laptops(103);
		Laptops l4 = new Laptops(104);
		Laptops l5 = new Laptops(105);
		
		Accessories a1 = new Accessories(201);
		Accessories a2 = new Accessories(202);
		Accessories a3 = new Accessories(203);
		Accessories a4 = new Accessories(204);
		Accessories a5 = new Accessories(205);
		Accessories a6 = new Accessories(206);
		Accessories a7 = new Accessories(207);
		Accessories a8 = new Accessories(208);
		Accessories a9 = new Accessories(209);
		Accessories a10 = new Accessories(210);
		
		//get the count of laptops and accessories created.
		System.out.println(Laptops.laptop_quantity+ " Laptops in Inventory");
		System.out.println(Accessories.accessories_quantity+ " Accessories in Inventory");
		
		//read the requirements of customer.
		Scanner sc =  new Scanner(System.in);
		System.out.println("Enter the number of laptops required: ");
		int laptops_need = sc.nextInt();
		System.out.println("Enter the number of accessories required: ");
		int accessories_need = sc.nextInt();
		
		//check if the needed quantity meets the requirements or not.
		if(laptops_need<Laptops.laptop_lowOrderLevelQuantity)
			System.out.println("RFM(Request for materia)");
		else if(laptops_need<=Laptops.laptop_quantity)
		{
			System.out.println("--LAPTOP INVOICE--");
			System.out.println(laptops_need+" laptops  has been ordered and\nwill be delieverd within 7 days.");
			System.out.println(laptops_need+" x 60000 = Rs. "+60000*laptops_need);
		}
		else
			System.out.println("Only "+Laptops.laptop_quantity+" laptops is avaialble in the Inventory.");
		
		if(accessories_need<Accessories.accessories_lowOrderLevelQuantity)
			System.out.println("RFM(Request for materia)");
		else if(accessories_need<=Accessories.accessories_quantity)
		{
			System.out.println("\n--ACCESSORIES INVOICE--");
			System.out.println(accessories_need+" accessories has been ordered and\nwill be delieverd within 7 days.");
			System.out.println(accessories_need+" x 10000 = Rs. "+10000*accessories_need);
		}
		else
			System.out.println("Only "+Accessories.accessories_quantity+" accessories is avaialble in the Inventory.");
		
		
	}

}
